package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.ManageWebs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Sql.CRUD;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Web;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Utils;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;
import com.iesportada.semipresencial.pmdm03gestiondedatos.databinding.ActivityEditWebBinding;

public class EditWebActivity extends AppCompatActivity {

    private ActivityEditWebBinding binding;
    private CRUD crud;
    private EditText editWebNameET, editLinkET, editMailET, editCategoryET, editImageET;
    private Button saveEditWeb, cancelEditWeb;
    private Web web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_web);
        binding = ActivityEditWebBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            finish();
            return;
        }

        crud = new CRUD(EditWebActivity.this);
        Long id = bundle.getLong("id");
        String nombre = bundle.getString("nombre"),
              link = bundle.getString("link"),
              email = bundle.getString("email"),
              categoria = bundle.getString("categoria"),
              imagen = bundle.getString("imagen");

        web = new Web(nombre, link, email, categoria, imagen, id);

        editWebNameET = binding.EditWebNameET;
        editLinkET = binding.EditLinkET;
        editMailET = binding.EditMailET;
        editCategoryET = binding.EditCategoryET;
        editImageET = binding.EditImageET;
        cancelEditWeb = binding.cancelEditWeb;
        saveEditWeb = binding.saveEditWeb;

        editWebNameET.setText(web.getNombre());
        editLinkET.setText(web.getLink());
        editMailET.setText(web.getEmail());
        editCategoryET.setText(web.getCategoria());
        editImageET.setText(web.getImagen());

        cancelEditWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        saveEditWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editWebNameET.setError(null);
                editLinkET.setError(null);
                editMailET.setError(null);
                editCategoryET.setError(null);
                editImageET.setError(null);

                String newName = editWebNameET.getText().toString(),
                        newLink = editLinkET.getText().toString(),
                        newMail = editMailET.getText().toString(),
                        newCategory = editCategoryET.getText().toString(),
                        newImage = editImageET.getText().toString();

                if (newName.isEmpty()){
                    newName = web.getNombre();
                }
                if(newLink.isEmpty()){
                    newLink = web.getLink();
                }
                if(newMail.isEmpty()){
                    newMail = web.getEmail();
                }
                if(newCategory.isEmpty()){
                    newCategory = web.getCategoria();
                }
                if (newImage.isEmpty()){
                    newImage = web.getImagen();
                }

                Web updatedWeb = new Web(newName, newLink, newMail, newCategory, newImage, web.getId());
                int updateLine = crud.updateWeb(updatedWeb);
                if(updateLine != 1){
                    Utils.showMessage("Error saving on db", EditWebActivity.this);
                }
                else{
                    finish();
                }
            }


        });

    }
}