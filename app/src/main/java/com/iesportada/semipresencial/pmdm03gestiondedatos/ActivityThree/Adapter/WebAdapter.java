package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Web;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;

import java.util.List;

public class WebAdapter extends RecyclerView.Adapter<WebAdapter.MyViewHolder> {

    private List<Web> webList;
    Context context;

    public void setWebList(List<Web> webList){
        this.webList = webList;
    }
    public WebAdapter(Context context, List<Web> web){
        this.context = context;
        this.webList = web;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i){
        View itemWeb = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_web, viewGroup, false);
        return new MyViewHolder(itemWeb);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Web web = webList.get(position);

        holder.nameTV.setText(web.getNombre());
        holder.linkTV.setText(web.getLink());
        holder.mailTV.setText(web.getEmail());
        holder.categoryTV.setText(web.getCategoria());
        holder.idTV.setText(String.valueOf(web.getId()));

        /**
         * Desde android 9 http está deshabilitado. Por alguna razón que no llego a comprender, en ocasiones glide
         * si coge la imagen y otras no, aún teniendo el permiso de android:usesCleartextTraffic="true" en el manifest.xml habilitado.
         */
        Glide.with(context).load(web.getImagen()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return webList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nameTV, linkTV, mailTV, categoryTV, idTV;
        ImageView imageView;

        MyViewHolder(View itemView){
            super(itemView);
            this.nameTV = itemView.findViewById(R.id.nameTV);
            this.linkTV =  itemView.findViewById(R.id.linkTV);
            this.categoryTV =  itemView.findViewById(R.id.categoryTV);
            this.mailTV =  itemView.findViewById(R.id.mailTV);
            this.idTV =  itemView.findViewById(R.id.idTV);
            this.imageView =  itemView.findViewById(R.id.imageView);
        }
    }
}
