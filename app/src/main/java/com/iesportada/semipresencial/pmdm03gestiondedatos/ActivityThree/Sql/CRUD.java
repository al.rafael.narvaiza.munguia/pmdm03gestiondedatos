package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Utils.Constants;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Web;

import java.util.ArrayList;

public class CRUD {
    DatabaseHelper databaseHelper;

    public CRUD(Context context){
        databaseHelper = new DatabaseHelper(context);
    }

    public long addWebToDB(Web web){
        //getWritableDatabase() para editar db.
        SQLiteDatabase sql = databaseHelper.getWritableDatabase();
        ContentValues insertableValues = new ContentValues();
        insertableValues.put("nombre", web.getNombre());
        insertableValues.put("link", web.getLink());
        insertableValues.put("email", web.getEmail());
        insertableValues.put("categoria", web.getCategoria());
        insertableValues.put("imagen", web.getImagen());
        return sql.insert(Constants.TABLE_NAME, null, insertableValues);
    }



    public ArrayList<Web> getWebs(){
        //getReadableDatabase() para leer de la db.
        ArrayList<Web> webArrayList = new ArrayList<>();
        SQLiteDatabase sql = databaseHelper.getReadableDatabase();
        String[] rowsToRead = {"nombre", "link", "email", "categoria", "imagen", "id"};
        Cursor cursor = sql.query(
                Constants.TABLE_NAME,
                rowsToRead,
                null,
                null,
                null,
                null,
                null

        );
        if(cursor == null){
            return webArrayList;
        }
        if(!cursor.moveToFirst()) return webArrayList;
        do{
            String getNameFromDB = cursor.getString(0);
            String getLinkFromDB = cursor.getString(1);
            String getEmailFromDB = cursor.getString(2);
            String getCategoryFromDB = cursor.getString(3);
            String getImageFromDB = cursor.getString(4);
            Long getIDFromDB = cursor.getLong(5);

            Web web = new Web(getNameFromDB, getLinkFromDB, getEmailFromDB, getCategoryFromDB, getImageFromDB, getIDFromDB);
            webArrayList.add(web);
        }while (cursor.moveToNext());

        cursor.close();

        return webArrayList;
    }

    public int updateWeb(Web web){
        SQLiteDatabase sql = databaseHelper.getWritableDatabase();
        ContentValues updateValues = new ContentValues();
        updateValues.put("nombre", web.getNombre());
        updateValues.put("link", web.getLink());
        updateValues.put("email", web.getEmail());
        updateValues.put("categoria", web.getCategoria());
        updateValues.put("imagen", web.getImagen());

        String[] updateArgs = {String.valueOf(web.getId())};

        return sql.update(Constants.TABLE_NAME, updateValues, Constants.ID, updateArgs);
    }

    public int deleteWeb(Web web){
        SQLiteDatabase sql = databaseHelper.getWritableDatabase();
        String[] deleteArgs = {String.valueOf(web.getId())};
        return sql.delete(Constants.TABLE_NAME, Constants.ID, deleteArgs);
    }

}
