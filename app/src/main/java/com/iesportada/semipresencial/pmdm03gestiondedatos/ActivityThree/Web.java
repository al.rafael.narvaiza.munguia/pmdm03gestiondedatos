package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree;

public class Web {
    private String nombre;
    private String link;
    private String email;
    private String categoria;
    private String imagen;
    private Long id;

    //Constructor del objeto.

    public Web(String nombre,String link,String email,String categoria,String imagen){
        this.setNombre(nombre);
        this.setLink(link);
        this.setEmail(email);
        this.setCategoria(categoria);
        this.setImagen(imagen);
    }

    //Constructor para la BD.

    public Web(String nombre,String link,String email,String categoria,String imagen, Long id){
        this.setNombre(nombre);
        this.setLink(link);
        this.setEmail(email);
        this.setCategoria(categoria);
        this.setImagen(imagen);
        this.setId(id);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
