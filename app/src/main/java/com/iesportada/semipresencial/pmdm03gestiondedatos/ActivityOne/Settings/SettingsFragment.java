package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne.Settings;

/**
 * @author Rafa Narvaiza
 * SettingsFragment es el encargado de cargar los fragments del "settings" y "about" en relación con la selección de header que hagamos.
 * En el caso de levantar el fragment "settings" se va a asignar un código HEX al color elegido de la lista, así como el encargado de asignar el valor de cambio de dolar a euro recogido de un editTextPreference.
 * En el caso de levantar el fragment "about" se va a mostrar la información de la aplicación.
 */

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;
import androidx.annotation.Nullable;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne.MainActivity1;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;

public class SettingsFragment extends PreferenceFragment {

    public static final String PREF_COLOR_SETTINGS = "list_preference_color_choose";
    public static final String PREF_CURRENCY_VALUE = "edit_text_preference_dolar_value";
    public static final String STORED_BLUE_COLOR = "Azul";
    public static final String STORED_GREEN_COLOR = "Verde";
    public static final String STORED_YELLOW_COLOR = "Amarillo";
    public static final String STORED_ORIGINAL_COLOR = "Original";
    private OnSharedPreferenceChangeListener preferenceChangeListener;
    private EditTextPreference preferenceCurrency;
    private static ListPreference preferenceColor;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        String settings = getArguments().getString("settings");
        if (settings.equals("general")){
            addPreferencesFromResource(R.xml.general_settings_preferences);
        }
        else if(settings.equals("about")){
            addPreferencesFromResource(R.xml.about_app_settings_preferences);
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferenceColor = (ListPreference) getPreferenceScreen().findPreference(PREF_COLOR_SETTINGS);
        preferenceCurrency = (EditTextPreference) getPreferenceScreen().findPreference(PREF_CURRENCY_VALUE);

        PreferenceManager.setDefaultValues(getActivity(), R.xml.general_settings_preferences, false);

        preferenceChangeListener = (sharedPreferences, key) -> {

            if (key.equals(PREF_COLOR_SETTINGS)){
                String color = sharedPreferences.getString(PREF_COLOR_SETTINGS, "0");
                MainActivity1.changeBackgroundColor(onColorChanged(color));
                preferenceColor.setDefaultValue(color);

            }
            if (key.equals(PREF_CURRENCY_VALUE)){
                MainActivity1.setRate(Double.parseDouble(sharedPreferences.getString(PREF_CURRENCY_VALUE, "0")));
                Toast.makeText(getActivity(), "Dolar value setted to: " + sharedPreferences.getString(PREF_CURRENCY_VALUE, "0"), Toast.LENGTH_SHORT).show();
                preferenceCurrency.setDefaultValue(sharedPreferences.getString(PREF_CURRENCY_VALUE, "0"));
            }
        };
    }

    public static Integer onColorChanged(String color){
        Integer colorID = null;
        switch (color){
            case STORED_BLUE_COLOR:
                colorID = Color.parseColor("#749EB2");
                break;
            case STORED_GREEN_COLOR:
                colorID = Color.parseColor("#AAD6A0");
                break;
            case STORED_YELLOW_COLOR:
                colorID = Color.parseColor("#FAD02C");
                break;
            case STORED_ORIGINAL_COLOR:
                colorID = Color.parseColor("#00FFFFFF");
        }
        return colorID;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }



}
