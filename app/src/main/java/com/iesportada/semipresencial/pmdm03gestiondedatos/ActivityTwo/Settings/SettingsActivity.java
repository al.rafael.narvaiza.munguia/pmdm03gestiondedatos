package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Settings;


import android.preference.PreferenceActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;

import java.util.List;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onBuildHeaders(List<Header> target){
        loadHeadersFromResource(R.xml.header_preferences2, target);
    }

    @Override
    protected boolean isValidFragment(String FragmentName){
        return SettingsFragment2.class.getName().equals(FragmentName);
    }
}