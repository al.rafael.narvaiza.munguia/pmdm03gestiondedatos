package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.List;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Alarm;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Utils;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;

import java.util.ArrayList;

public class CustomListActivity extends AppCompatActivity {

    Utils utils = new Utils();
    ArrayList<Alarm> arrayOfAlarmsDisplayer = null;
    private Button buttonSubmitEditAlarm;
    private Button buttonDeleteAlarm;
    private TextView editTimeAlarmDialog;
    private TextView editDescriptionAlarmDialog;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list);
        populateUsersList();
        
        
    }

    private void populateUsersList() {

        //Construimos la fuente de datos;

        arrayOfAlarmsDisplayer = Utils.recoverAlarmsFromSD();

        //Creamos el adaptador para convertir el array en vista

        AlarmAdapter adapter = new AlarmAdapter(this, arrayOfAlarmsDisplayer);

        //Adjuntamos el adaptador a un ListView

        ListView listView = (ListView) findViewById(R.id.lvAlarms);
        listView.setAdapter(adapter);

        //Levantamos un listener para poder editar las alarmas

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id){
                showCustomDialog(pos, arrayOfAlarmsDisplayer.get(pos).getTime(), arrayOfAlarmsDisplayer.get(pos).getDescription(), arrayOfAlarmsDisplayer.get(pos).getSound());
                return  true;
            }
        });
    }

    //Método para mostrar el dialogo de edición.

    private void showCustomDialog(int pos, Long time, String description, String sound) {
        final Dialog dialog = new Dialog(CustomListActivity.this);


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);

        dialog.setContentView(R.layout.custom_dialog);

        editDescriptionAlarmDialog = dialog.findViewById(R.id.editDescriptionAlarmDialog);
        editDescriptionAlarmDialog.setText(arrayOfAlarmsDisplayer.get(pos).getDescription());
        editTimeAlarmDialog = dialog.findViewById(R.id.editTimeAlarmDialog);
        editTimeAlarmDialog.setText((String.valueOf(arrayOfAlarmsDisplayer.get(pos).getTime()/1000)));
        buttonSubmitEditAlarm = dialog.findViewById(R.id.buttonSubmitEditAlarm);
        buttonDeleteAlarm = dialog.findViewById(R.id.buttonDeleteAlarm);

        buttonSubmitEditAlarm.setOnClickListener(view -> {
            String timeFromDialog = editTimeAlarmDialog.getText().toString();
            String descriptionFromDialog = editDescriptionAlarmDialog.getText().toString();
            if(timeFromDialog.isEmpty()){
                timeFromDialog = String.valueOf(time);
            }
            if(descriptionFromDialog.isEmpty()){
                descriptionFromDialog = description;
            }
            editAlarm(pos, (Long.valueOf(timeFromDialog)*1000), descriptionFromDialog, sound);

            dialog.dismiss();
        });

        buttonDeleteAlarm.setOnClickListener(view ->{
            deleteAlarm(pos);

            dialog.dismiss();
        });
        dialog.show();

    }

    private void deleteAlarm(int pos) {
        arrayOfAlarmsDisplayer.remove(pos);
        utils.writeAlarmsOnSD(arrayOfAlarmsDisplayer);

    }


    public void editAlarm(int pos, Long time, String description, String sound){
        //Creamos una nueva instancia de la alarma que queremos editar y la seteamos en la colección.

        Alarm alarm = new Alarm();
        alarm.setTime(time);
        alarm.setDescription(description);
        alarm.setSound(sound);
        arrayOfAlarmsDisplayer.set(pos, alarm);

        //Pasamos a escribir la colección de nuevo en la SD para poder tenerla disponible.

        utils.writeAlarmsOnSD(arrayOfAlarmsDisplayer);
    }


}