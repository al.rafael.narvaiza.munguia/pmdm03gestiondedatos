package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Adapter.WebAdapter;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.ManageWebs.AddWebActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.ManageWebs.EditWebActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Sql.CRUD;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Utils.*;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;
import com.iesportada.semipresencial.pmdm03gestiondedatos.databinding.ActivityMain3Binding;


import java.util.ArrayList;
import java.util.List;

public class Main3Activity extends AppCompatActivity {

    private ActivityMain3Binding binding;
    private List<Web> webList;
    private RecyclerView recyclerView;
    private WebAdapter webAdapter;
    private CRUD crud;
    private FloatingActionButton fabAddWeb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityMain3Binding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main_3);
        View view = binding.getRoot();
        setContentView(view);

        crud = new CRUD(this);
        recyclerView = binding.recyclerViewWebs;

        fabAddWeb = binding.fabAdd;


        webList = new ArrayList<>();
        webList= Sandbox.webList();

        //Para facilitar la visualización del programa, se crea este método.

        if(crud.getWebs().isEmpty()){
            populateSQL();
        }


        webAdapter = new WebAdapter(this,webList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(webAdapter);

        webListUpdate();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Web selectedWeb = webList.get(position);
                Intent intent = new Intent(Main3Activity.this, EditWebActivity.class);
                intent.putExtra("id", selectedWeb.getId());
                intent.putExtra("nombre", selectedWeb.getNombre());
                intent.putExtra("link", selectedWeb.getLink());
                intent.putExtra("email", selectedWeb.getEmail());
                intent.putExtra("categoria", selectedWeb.getCategoria());
                intent.putExtra("imagen", selectedWeb.getImagen());

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

                final Web deletedWeb = webList.get(position);

                Context context;
                AlertDialog dialog = new AlertDialog
                        .Builder(Main3Activity.this)
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                crud.deleteWeb(deletedWeb);
                                webListUpdate();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                               dialogInterface.dismiss();
                            }
                        })
                        .setTitle("Confirm")
                        .setMessage("Delete web " + deletedWeb.getNombre() + " ?")
                        .create();
                dialog.show();
            }
        }));

        fabAddWeb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(v.getContext(), AddWebActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        webListUpdate();
    }

    public boolean populateSQL() {
        for (Web i : webList){
            crud.addWebToDB(i);
        }
        return true;
    }

    public void webListUpdate(){
        if(webAdapter == null)return;
        webList = crud.getWebs();
        webAdapter.setWebList(webList);
        webAdapter.notifyDataSetChanged();
    }


}