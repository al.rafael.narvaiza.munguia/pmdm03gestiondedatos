package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Utils;

public class Constants {

    public static final String
            DB_NAME = "webs",
            TABLE_NAME = "webs",
            ID = "id = ?",
            CATEGORY_ONE = "CSGO Skins bet",
            CATEGORY_TWO = "CSGO skins market";
    public static final int DB_VERSION = 1;

}
