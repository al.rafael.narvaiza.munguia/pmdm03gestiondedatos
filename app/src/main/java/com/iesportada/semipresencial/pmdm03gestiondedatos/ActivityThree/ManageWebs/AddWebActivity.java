package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.ManageWebs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Main3Activity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Sql.CRUD;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Web;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Utils;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;
import com.iesportada.semipresencial.pmdm03gestiondedatos.databinding.ActivityAddWebBinding;

public class AddWebActivity extends AppCompatActivity {

    private ActivityAddWebBinding binding;
    private Button saveButton, cancelButton, loadDefaultButton;
    private EditText addNameEditText, addLinkEditText, addMailEditText, addCategoryEditText, addImageEditText;
    private CRUD crud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_web);
        binding = ActivityAddWebBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        loadDefaultButton = binding.loadDefaultButton;
        saveButton = binding.saveWebButton;
        cancelButton = binding.cancelAddWebButton;
        addNameEditText = binding.addNameTE;
        addLinkEditText = binding.addLinkTE;
        addMailEditText = binding.addMailTE;
        addCategoryEditText = binding.addCategoryTE;
        addImageEditText = binding.addImageLinkTE;

        crud = new CRUD(AddWebActivity.this);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNameEditText.setError(null);
                addLinkEditText.setError(null);
                addMailEditText.setError(null);
                addCategoryEditText.setError(null);
                addImageEditText.setError(null);

                String name = addNameEditText.getText().toString(),
                        link = addLinkEditText.getText().toString(),
                        mail = addMailEditText.getText().toString(),
                        category = addCategoryEditText.getText().toString(),
                        image = addImageEditText.getText().toString();

                if("".equals(name)){
                    addNameEditText.setError("Please type web name");
                    addNameEditText.requestFocus();
                    return;
                }
                if("".equals(link)){
                    addLinkEditText.setError("Please type web link");
                    addLinkEditText.requestFocus();
                    return;
                }
                if("".equals(mail)){
                    addMailEditText.setError("Please type web mail");
                    addMailEditText.requestFocus();
                    return;
                }
                if("".equals(category)){
                    addCategoryEditText.setError("Please type web category");
                    addCategoryEditText.requestFocus();
                    return;
                }
                if("".equals(image)){
                    addImageEditText.setError("Please input image link");
                    addImageEditText.requestFocus();
                    return;
                }
                Web newWeb = new Web(name, link, mail, category, image);
                long id = crud.addWebToDB(newWeb);
                if (id == -1){
                    Utils.showMessage("Error trying to save on db, please try again.", AddWebActivity.this);
                }
                else {
                    finish();
                }
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}