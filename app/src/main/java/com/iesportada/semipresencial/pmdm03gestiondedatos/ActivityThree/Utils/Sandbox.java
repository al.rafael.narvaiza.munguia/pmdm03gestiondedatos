package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Utils;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Web;

import java.util.ArrayList;
import java.util.List;

public class Sandbox {

    public static List<Web> webList(){
        List<Web> webList = new ArrayList<>();
        webList.add(new Web("CSGO Empire", "https://csgoempire.com/", "mail@csgoempire.com", Constants.CATEGORY_ONE, "https://paginasdeapuestascsgo.com/wp-content/uploads/codigo-csgoempire-1.jpg"));
        webList.add(new Web("ThunderPick", "https://thunderpick.com/en/games/crash", "contact@thunderpick.com", Constants.CATEGORY_ONE, "https://pbs.twimg.com/profile_images/1296040221331652608/PB1XS52S.jpg"));
        webList.add(new Web("CSGO fast", "https://csgofast123.com/game/classic", "mail@csgofast132.com", Constants.CATEGORY_ONE, "https://csgobettings.com/wp-content/uploads/2019/10/csgofast.jpg"));
        webList.add(new Web("BITSKINS", "https://bitskins.com/", "contact@bitskins.com", Constants.CATEGORY_TWO, "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/07/07fb269761fe82dcd3a3d0bb9cbd4ea04cd1fad3_full.jpg"));
        webList.add(new Web("StoneFire", "https://stonefire.io/search?game=730&q=&currency=EUR", "contact@stonefire.io", Constants.CATEGORY_TWO, "https://pbs.twimg.com/profile_images/650980475226484737/ruzSnU_a.png"));
        return webList;
    }


}
