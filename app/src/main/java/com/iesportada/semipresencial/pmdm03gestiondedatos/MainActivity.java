package com.iesportada.semipresencial.pmdm03gestiondedatos;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne.MainActivity1;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityThree.Main3Activity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.MainActivity2;
import com.iesportada.semipresencial.pmdm03gestiondedatos.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View view = binding.getRoot();
        setContentView(view);

        binding.Activity1Button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, MainActivity1.class);
            startActivity(intent);
        });

        binding.Activity2Button.setOnClickListener(v ->{
            Intent intent = new Intent(MainActivity.this, MainActivity2.class);
            startActivity(intent);
        });

        binding.Activity3Button.setOnClickListener(v->{
            Intent intent = new Intent(MainActivity.this, Main3Activity.class);
            startActivity(intent);
        });
    }
}