package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne.Settings;

/**
 * @author Rafa Narvaiza
 * SettingsActivity es el encargado de cargar el header_preference.
 */

import android.preference.PreferenceActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;

import java.util.List;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onBuildHeaders(List<PreferenceActivity.Header> target){
        loadHeadersFromResource(R.xml.header_preference, target);
    }

    @Override
    protected boolean isValidFragment(String FragmentName){
        return SettingsFragment.class.getName().equals(FragmentName);
    }
}