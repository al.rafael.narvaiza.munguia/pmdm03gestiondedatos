package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.List;

import android.app.AlarmManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Alarm;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;


import java.util.ArrayList;

public class AlarmAdapter extends ArrayAdapter<Alarm> {
    public AlarmAdapter(Context context, ArrayList<Alarm> alarms){
        super(context, 0, alarms);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        //recogemos los datos de la alarma para una posicion

        Alarm alarm = getItem(position);

        //Comprobamos si hay alguna vista en uso, sino inflamos la vista

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_alarm, parent, false);
        }

        //Buscamos la vista de los items para rellenarlos.
        TextView tvTime = (TextView) convertView.findViewById(R.id.tvTime);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        TextView tvSound = (TextView) convertView.findViewById(R.id.tvSound);

        //rellenamos la vista con datos

        tvTime.setText(String.valueOf(alarm.getTime()/1000) + " segundos");
        tvDescription.setText(alarm.getDescription());
        tvSound.setText(alarm.getSound());


        //Devolvemos la vista completada para desplegarla en pantalla
        return convertView;

    }

}
