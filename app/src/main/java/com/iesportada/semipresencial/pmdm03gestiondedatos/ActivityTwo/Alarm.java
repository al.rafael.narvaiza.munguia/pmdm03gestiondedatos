package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo;

public class Alarm {
    private Long time;
    private String sound;
    private String description;

    public Alarm(){
        super();
    }

    public Alarm(Long time, String description, String sound){
        this.setTime(time);
        this.setDescription(description);
        this.setSound(sound);
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "time=" + time +
                ", sound='" + sound + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
